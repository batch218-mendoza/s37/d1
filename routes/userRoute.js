//dependencies

const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController =  require("../controllers/userController.js");
const auth = require("../auth.js");



// FOR ACTIVITY S41
router.post("/enroll", auth.verify, (req, res) =>{

	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.enroll(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

router.post("/checkEmail", (req,res) =>{
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


//FOR TESTING
router.post("/details1",(req,res) =>{
	userController.getProfile1(req.body).then(result =>
		res.send(result));
})


// for activty s38

router.get("/details/:id", (req,res) => {
userController.getProfile(req.params.id, req.body).then(result => res.send(result));
});



router.get("/alluser", (req,res) => {
userController.getAllUser().then(result => res.send(result));
});




// FOR ACTIVITY S41



module.exports = router;

	
