const express = require("express"); // accessing package of express
const router = express.Router(); // use dot notation to access content of a package
const auth = require("../auth.js");
const Course = require("../models/course.js");

const courseController = require("../controllers/courseController.js");



//FOR S39-S40 ACTIVITY add course by admin
router.get("/addCourseAdmin", auth.verify, (req, res) =>{
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourseAdmin(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

/*
//Create a single course
router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(
		resultFromController => res.send(resultFromController))
})*/

router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})


router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})


router.get("/:courseId", (req, res) => {
								//to get "params" courseID from the URL
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})



router.patch("/:courseId/update", auth.verify, (req, res) =>{
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})




//FOR S39-S40 ACTIVITY archive

router.put("/:courseId/archive", auth.verify, (req, res) =>{
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archive(req.params.courseId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})


module.exports = router;
