/*
	npm init -y
	npm install express
	npm intall mongoose
	npm install cors
	npm install bcrypt
	npm install jsonwebtoken

*/


const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute = require("./routes/userRoute.js");
const courseRoute = require("./routes/courseRoute.js");


// to create a express server/application
const app = express();


//Middlewares - allows to bridge our backend application(server) to our front end
// to allow cross origin resource sharing
app.use(cors());
//use to read json object
app.use(express.json());
//use to read to forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/courses", courseRoute);
// connect to our mongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.tcls8la.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
	});

// prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Mendoza-Mongo DB Atlas'));

app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000 }`)});