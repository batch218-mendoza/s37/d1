// to connect to user model
const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/course.js");

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a value from a collection
	return User.find({email:reqBody.email}).then(result =>{
		if(result.length > 0){
			return true;

		}
		// condition if thre is no existing user
		else{
				return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// bcrypt - package for password hashing
		//hashSync - synchrously generate a hash
		// hash- asynchronously generate a hash
		password: bcrypt.hashSync(reqBody.password, 10),
		// 10 = salt rounds
		// salt rounds is proportional to hashing rounds,
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
				if(result == null){
					return false
				}
		else{
			//compareSynce is bcrypt function to compare a hashed password to unhashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)};
				}
				else{
					// if password do not match
					return false;
				}

		}
	})
}

// FOR TESTING
module.exports.getProfile1 = (reqBody) => {
	return User.findOne({_id : reqBody.id}).then(result => {
			if(reqBody.id == result._id){
				result.password = "";
				return result;
			}

		else{
			return "wrong ID";
		}
	})
};



// for activty s38
module.exports.getProfile = (taskId, newContent) => {
	return User.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
		result.password = " ";
		return result;
}
	
	})
};

// FOR ACTIVITY S38
module.exports.getAllUser= () => {
	return User.find().then((result) => {
		return result;
	})
}







// FOR ACTIVITY S41

// ENROLL USER TO A CLASS



module.exports.enroll = async (data, newData) => {
		
if(newData.isAdmin == true){
	let isUserUpdated = await User.findById(data.userId).then(user => {

		
		user.enrollments.push({courseId : data.courseId});

	
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId});

		
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});


		if(isUserUpdated && isCourseUpdated){
		return true;
	
	} else {
		return false;
	};
}else {
			let message = Promise.resolve('User must be ADMIN to access this');
			return message.then((value) => {return value})

};

}

	

	








