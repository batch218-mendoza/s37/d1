const Course = require("../models/course.js");

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})
	return newCourse.save().then((newCourse, error) => {
		if(error){
				return error;
		}
		else{
			return newCourse;
		}
	})
}

module.exports.getAllCourse = () => {
	return Course.find({/*document*/}).then(result => {
	return result;
})
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

//Get specific course
module.exports.getCourse = (courseId) => {
							//inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
}

//update a course admin only
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId , {
			name: newData.course.name,
			description: newData.course.description,
			price: newData.course.price,
		}).then((updatedCourse, error) => {
			if(error){
				return false;
			}
			
			return true;
		
		})

		}
		else {
			let message = Promise.resolve('User must be ADMIN to access this');
			return message.then((value) => {return value})
	}

}


//FOR S39-S40 ACTIVITY add course by admin


module.exports.updateCourseAdmin = (reqBody, newData) => {
if(newData.isAdmin == true){
	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})
	return newCourse.save().then((newCourse, error) => {
		if(error){
				return error;
		}
		else{
			return newCourse;
		}
	})
}else{
	let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
}
}

//FOR S39-S40 ACTIVITY archive 

module.exports.archive = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId , {
			isActive: newData.course.isActive
		}).then((updatedCourse, error) => {
			if(error){
				return false;
			}
			
			return true;
		
		})

		}
		else {
			let message = Promise.resolve('User must be ADMIN to access this');
			return message.then((value) => {return value})
	}

}