const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
			firstName:{
				type: String,
				required: [true, "firstname is required"]
			},
			lastName:{
				type: String,
				required: [true, "lastname is required"]
			},
			email:{
				type: String,
				required: [true, "email is required"]
			},
			password:{
				type: String,
				required: [true, "password is required"]
			},
			isAdmin:{
				type: Boolean,
				default: false,
			},
			mobileNo:{
				type: String,
				required: [true, "mobile number is required"]
			},
			enrollments: [
				{
					courseId: {
						type: String,
						required: [true, "courseId is required"]
					},
					enrolledOn: {
						type: Date,
					default: new Date(),

					},
			
}
				]
})

									//model  //schema
module.exports = mongoose.model("User", userSchema)