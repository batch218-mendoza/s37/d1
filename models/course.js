// use to create schema
const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name:{
		type: String,
		// Requires the data to be included when creating a record
		// the "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not
		required: [true, "Course is required"]
	},
		description: {	
			type: String,
			required: [true, "Description is requred"]
		},

		price : {
			type: Number,
			required: [true, "Price is required"]
		},

		isActive: {
			type: Boolean,
			default:true
		},

		createdOn: {
			type: Date,
			//the "new date expression create a new date that current date and time whenever a course is created in our database"
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "UserId is required"]
				},
				enrolledOn: {
					type: Date,
				default: new Date(),

				}
			}


			]
})

									//model
module.exports = mongoose.model("Course", courseSchema)